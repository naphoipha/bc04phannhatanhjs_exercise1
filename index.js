// Exercise 1
/**
 * /**
 * input: 
 * a = số ngày làm của nhân viên
 * const b = 100000
 * 
 * todo:
 * lương nhân viên = a x b
 * 
 * output:
 * lương nhân viên 
 */

var day = 5;
const payDay = 100000;

var pay = day*payDay;
console.log("Lương nhân viên: ", pay);

//Exercise 2
/**
 * input: 5 số thực a,b,c,d,e
 * 
 * todo: giá trị trung bình = (a+b+c+d+e)/5
 *
 * output: giá trị trung bình 
 */

 var soThuc1 = 2;
 var soThuc2 = 4;
 var soThuc3 = 6;
 var soThuc4 = 8;
 var soThuc5 = 10;
 
 var average = (soThuc1+soThuc2+soThuc3+soThuc4+soThuc5)/5;
 console.log("Giá trị trung bình = ", average);

 //Exercise 3
 /**
 * input: 
 * a = số tiền USD
 * 1 USD = 23.500
 * const b = 23.500
 * 
 * todo:
 * quy đổi ra VND:
 * a*b
 * 
 * output:
 * số tiền VND
 */

var sotien = 199;
const usd = 23500;

var output = sotien*usd;
console.log("Số tiền VND = ", output, "VND");

//Exercise 4
/**
 * input: 
 * a = chiều dài
 * b = chiều rộng
 * 
 * todo:
 * chu vi hình chữ nhật = (a+b)*2
 * diện tích = a*b
 * 
 * output:
 * chu vi hình chữ nhật và diện tích ra console log
 * 
 */

 var chieuRong = 4;
 var chieuDai = 10;
 
 var dienTich = chieuRong*chieuDai;
 var chuVi = (chieuRong+chieuDai)*2;
 
 console.log("Chu Vi: ", chuVi);
 console.log("Diện Tích: ", dienTich);

 //Exercise 5
 /**
 * input:
 * nhập 2 ký số 
 * a = 12
 * 
 * todo:
 * tổng 2 ký số : 1 + 2 = 3;
 * lấy hàng đơn vị: a % 10
 * lấy hàng chục: a / 10
 * 
 * output:
 * xuất ra tổng 2 ký số ra console log
 * 
 * 
 */

var soThuc = 12;
var hangChuc = Math.floor(soThuc/10);
var hangDonvi = soThuc % 10;

var tong = hangChuc + hangDonvi;
console.log("Tổng 2 ký số: ",tong);
